# pip install psutil

import psutil


interfaces = psutil.net_if_addrs()

interface_wifi = "wlp0s20f3"

adresse_ip_wifi = None

if interface_wifi in interfaces:
    # for carte in interfaces:
    #     print(carte)
    adresse_ip_wifi = interfaces[interface_wifi][0][1]

if adresse_ip_wifi:
    print(adresse_ip_wifi)
else:
    print(f"Adresse IP de l'interface WiFi `{interface_wifi}` non trouvée. \nModifiez interface_wifi (Ligne 7)")
    