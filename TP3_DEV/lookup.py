import socket
import sys

if len(sys.argv) < 2:
    print("Utilisation : python lookup.py <nom_de_domaine>")
    sys.exit(1)

nom_de_domaine = sys.argv[1]

try:
    adresse_ip = socket.gethostbyname(nom_de_domaine)
    print(adresse_ip)
except socket.gaierror as e:
    print(e)