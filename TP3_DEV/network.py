import os
import socket
import sys

import psutil
    # pip install psutil

global_interface_wifi = "wlp0s20f3"

def lookup(nom_de_domaine):

    try:
        adresse_ip = socket.gethostbyname(nom_de_domaine)
        return adresse_ip
    except socket.gaierror as e:
        return e
    
def ping( cible):

    ping = f"ping -c 1 {cible} >/dev/null 2>&1"
    reponse = os.system(ping)

    if reponse == 0:
        return "UP !"
    else:
        return "DOWN !"
    
def ip():
    interfaces = psutil.net_if_addrs()

    adresse_ip_wifi = None

    adresse_ip_wifi = interfaces[global_interface_wifi][0][1]


    if adresse_ip_wifi:
        return adresse_ip_wifi
    else:
        return f"Adresse IP de l'interface WiFi `{global_interface_wifi}` non trouvée. \nModifiez global_interface_wifi (Ligne 8)"
    

if sys.argv[1] == "lookup":
    to_print = lookup(sys.argv[2])
elif sys.argv[1] == "ping":
    to_print = ping(sys.argv[2])
elif sys.argv[1] == "ip":
    to_print = ip()
else :
    to_print = f"'{sys.argv[1]}' is not available command. Déso."
print(to_print)