import os
import sys

if len(sys.argv) < 2:
    print("Utilisation : python is_up.py <cible>")
    sys.exit(1)

cible = sys.argv[1]

ping = f"ping -c 1 {cible} >/dev/null 2>&1"
reponse = os.system(ping)

if reponse == 0:
    print("UP !")
else:
    print("DOWN !")