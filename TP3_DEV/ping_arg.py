import os
import sys

if len(sys.argv) < 2:
    print("Utilisation : python ping_arg.py <cible>")
    sys.exit(1)

cible = sys.argv[1]

ping = f"ping -c 1 {cible}"
os.system(ping)