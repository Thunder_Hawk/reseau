# Sujet Réseau et Infra

## I. Dumb switch

### 3. Setup topologie 1

```
PC1> ip 10.5.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.5.1.1 255.255.255.0

PC1> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC1    10.5.1.1/24          255.255.255.0     00:50:79:66:68:00  20006  127.0.0.1:20007
       fe80::250:79ff:fe66:6800/64

```

```
PC2> ip 10.5.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.5.1.2 255.255.255.0

PC2> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC2    10.5.1.2/24          255.255.255.0     00:50:79:66:68:01  20004  127.0.0.1:20005
       fe80::250:79ff:fe66:6801/64

```

```
PC1> ping 10.5.1.2

84 bytes from 10.5.1.2 icmp_seq=1 ttl=64 time=0.218 ms
84 bytes from 10.5.1.2 icmp_seq=2 ttl=64 time=0.198 ms


PC2> ping 10.5.1.1

84 bytes from 10.5.1.1 icmp_seq=1 ttl=64 time=0.147 ms
84 bytes from 10.5.1.1 icmp_seq=2 ttl=64 time=0.243 ms
```

> That's works !


## II. VLAN

### 3. Setup topologie 2


```
PC1> ip 10.5.10.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.5.10.1 255.255.255.0

PC1> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=0.139 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=0.370 ms

PC1> ping 10.5.10.3

84 bytes from 10.5.10.3 icmp_seq=1 ttl=64 time=0.418 ms
84 bytes from 10.5.10.3 icmp_seq=2 ttl=64 time=0.238 ms

```

```
PC2> ip 10.5.10.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.5.10.2 255.255.255.0

PC2> ping 10.5.10.1            

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=0.677 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=0.283 ms

PC2> ping 10.5.10.3 

84 bytes from 10.5.10.3 icmp_seq=1 ttl=64 time=0.138 ms
84 bytes from 10.5.10.3 icmp_seq=2 ttl=64 time=0.246 ms

```

```
PC3> ip 10.5.10.3 255.255.255.0
Checking for duplicate address...
PC3 : 10.5.10.3 255.255.255.0

PC3> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=0.483 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=0.282 ms

PC3> ping 10.5.10.1

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=0.136 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=0.224 ms
```

```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 10
IOU1(config-vlan)# name students
IOU1(config-vlan)#exit
IOU1(config)#vlan 20
IOU1(config-vlan)# name teachers
IOU1(config-vlan)#exit
IOU1(config)#interface ethernet0/1
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 10
IOU1(config-if)#exit
IOU1(config)#interface ethernet0/2    
IOU1(config-if)#switchport mode access   
IOU1(config-if)#switchport access vlan 10
IOU1(config-if)#exit
IOU1(config)#interface ethernet0/3    
IOU1(config-if)#switchport mode access   
IOU1(config-if)#switchport access vlan 20
IOU1(config-if)#exit
IOU1(config)#exit
IOU1#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
...
10   students                         active    Et0/1, Et0/2
20   teachers                         active    Et0/3
...
```

```
PC1> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=0.137 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=0.234 ms

PC1> ping 10.5.10.3

host (10.5.10.3) not reachable
```

```
PC2> ping 10.5.10.1

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=0.206 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=0.271 ms

PC2> ping 10.5.10.3

host (10.5.10.3) not reachable
```

```
PC3> ping 10.5.10.1

host (10.5.10.1) not reachable

PC3> ping 10.5.10.2

host (10.5.10.2) not reachable
```

> Yay !

## III. Routing

> PC1 & PC2 OK


`admin startup.vpc`
```
set pcname admin
ip 10.5.20.1 255.255.255.0 24
ip auto
```


```
[user@web1 ~]$ ip a
...
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8c:46:be brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fe8c:46be/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
...
[user@web1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
[user@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.5.30.1
NETMASK=255.255.255.0
[user@web1 ~]$ sudo systemctl restart NetworkManager
[user@web1 ~]$ sudo nmcli con up 'System enp0s3'
```



```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 10
IOU1(config-vlan)#name clients
IOU1(config-vlan)#exit
IOU1(config)#vlan 20
IOU1(config-vlan)#name admins
IOU1(config-vlan)#exit
IOU1(config)#vlan 30
IOU1(config-vlan)#name servers
IOU1(config-vlan)#exit
IOU1(config)#interface ethernet1/0
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 30
IOU1(config-if)#exit
IOU1(config)#exit
IOU1#
*Nov 10 13:53:02.373: %SYS-5-CONFIG_I: Configured from console by console
IOU1#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
...
10   clients                          active    Et0/1, Et0/2
20   admins                           active    Et0/3
30   servers                          active    Et1/0
...
```
```
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.  
R1(config)#interface fastEthernet 0/0.10  
R1(config-subif)#encapsulation dot1Q 10  
R1(config-subif)#ip addr 10.5.10.254 255.255.255.0  
R1(config-subif)#exit   
R1(config)#interface fastEthernet 0/0.20      
R1(config-subif)#encapsulation dot1Q 20             
R1(config-subif)#ip addr 10.5.20.254 255.255.255.0  
R1(config-subif)#exit  
R1(config)#interface fastEthernet 0/0.30      
R1(config-subif)#encapsulation dot1Q 30  
R1(config-subif)#ip addr 10.5.30.254 255.255.255.0  
R1(config-subif)#exit  
R1(config)#interface fastEthernet 0/0  
R1(config-if)#no sh  
R1(config-subif)#exit  
R1(config)#exit  
```

```
IOU1#conf t  
Enter configuration commands, one per line.  End with CNTL/Z.  
IOU1(config)#interface ethernet 0/0  
IOU1(config-if)#switchport  trunk encapsulation dot1q  
IOU1(config-if)#switchport trunk allowed vlan 10,20,30   
IOU1(config-if)#switchport mode trunk   
IOU1(config-if)#exit  
IOU1(config)#exit
```

```
PC1> ping 10.5.10.254

84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=9.782 ms  
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=6.701 ms

```

```
PC2> ping 10.5.10.254               

84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=9.089 ms  
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=7.145 ms
```

```
admin> ping 10.5.20.254           

84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=9.754 ms  
84 bytes from 10.5.20.254 icmp_seq=2 ttl=255 time=7.520 ms
```

```
[user@web1 ~]$ ping 10.5.30.254
PING 10.5.30.254 (10.5.30.254) 56(84) bytes of data.  
64 bytes from 10.5.30.254: icmp_seq=1 ttl=255 time=5.62 ms  
64 bytes from 10.5.30.254: icmp_seq=2 ttl=255 time=8.22 ms

```

```
PC1> ip 10.5.10.1/24 10.5.10.254   
Checking for duplicate address...  
PC1 : 10.5.10.1 255.255.255.0 gateway 10.5.10.254
```

```
PC2> ip 10.5.10.2/24 10.5.10.254   
Checking for duplicate address...  
PC2 : 10.5.10.2 255.255.255.0 gateway 10.5.10.254
```

```
admin> ip 10.5.20.1/24 10.5.20.254   
Checking for duplicate address...   
admin : 10.5.20.1 255.255.255.0 gateway 10.5.20.254
```

```
[user@web1 network-scripts]$ sudo ip route add default via 10.5.30.254

```

```
PC1> ping 10.5.20.1     

84 bytes from 10.5.20.1 icmp_seq=1 ttl=63 time=29.751 ms  
84 bytes from 10.5.20.1 icmp_seq=2 ttl=63 time=18.184 ms

PC1> ping 10.5.30.1

84 bytes from 10.5.30.1 icmp_seq=1 ttl=63 time=19.856 ms  
84 bytes from 10.5.30.1 icmp_seq=2 ttl=63 time=18.363 ms
```

```
PC2> ping 10.5.20.1

84 bytes from 10.5.20.1 icmp_seq=1 ttl=63 time=17.918 ms  
84 bytes from 10.5.20.1 icmp_seq=2 ttl=63 time=11.714 ms

PC2> ping 10.5.30.1

84 bytes from 10.5.30.1 icmp_seq=1 ttl=63 time=17.129 ms  
84 bytes from 10.5.30.1 icmp_seq=2 ttl=63 time=17.731 ms

```

```
admin> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=63 time=14.524 ms  
84 bytes from 10.5.10.2 icmp_seq=2 ttl=63 time=18.111 ms

admin> ping 10.5.30.1

84 bytes from 10.5.30.1 icmp_seq=1 ttl=63 time=11.366 ms  
84 bytes from 10.5.30.1 icmp_seq=2 ttl=63 time=11.368 ms
```

```
[user@web1 ~]$ ping 10.5.10.1  
PING 10.5.10.1 (10.5.10.1) 56(84) bytes of data.  
64 bytes from 10.5.10.1: icmp_seq=1 ttl=63 time=11.6 ms  
64 bytes from 10.5.10.1: icmp_seq=2 ttl=63 time=18.0 ms

[user@web1 ~]$ ping 10.5.20.1  
PING 10.5.20.1 (10.5.20.1) 56(84) bytes of data.   
64 bytes from 10.5.20.1: icmp_seq=1 ttl=63 time=21.8 ms  
64 bytes from 10.5.20.1: icmp_seq=2 ttl=63 time=16.8 ms
```

## IV. NAT

```
FastEthernet0/1            10.0.3.16       YES DHCP   up                    up  
```

```
PC1> ping 1.1.1.1      

84 bytes from 1.1.1.1 icmp_seq=2 ttl=62 time=15.046 ms   
84 bytes from 1.1.1.1 icmp_seq=3 ttl=62 time=19.086 ms   

```

```
PC2> ping 1.1.1.1

84 bytes from 1.1.1.1 icmp_seq=4 ttl=62 time=17.278 ms   
84 bytes from 1.1.1.1 icmp_seq=5 ttl=62 time=20.469 ms 
```

```
admin> ping 1.1.1.1

84 bytes from 1.1.1.1 icmp_seq=4 ttl=62 time=16.485 ms   
84 bytes from 1.1.1.1 icmp_seq=5 ttl=62 time=18.989 ms 
```

```
[user@web1 ~]$ ping 1.1.1.1
64 bytes from 1.1.1.1: icmp_seq=1 ttl=255 time=28.88 ms 
```

```
R1#conf t  
Enter configuration commands, one per line.  End with CNTL/Z.   
R1(config)#interface fastEthernet 1/0   
R1(config-if)#ip nat outside   
R1(config-if)#no sh   
*Mar  1 00:33:21.603: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0,    changed state to up   
R1(config-if)#interface fastEthernet 0/0   
R1(config-if)#ip nat inside  
R1(config-if)#exit  
R1(config)#access-list 1 permit any  
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload  
R1(config)#exit
```

```
PC1> ip dns 8.8.8.8
```

```
PC2> ip dns 8.8.8.8
```

```
admin> ip dns 8.8.8.8
```

```
[user@web1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3        
[user@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static              
ONBOOT=yes

IPADDR=10.5.30.1                
NETMASK=255.255.255.0                     
[user@web1 ~]$ sudo systemctl restart NetworkManager                       
[user@web1 ~]$ sudo nmcli con up 'System enp0s3'

DNS=8.8.8.8
```

```
PC1> ping google.com
google.com resolved to 216.58.214.78

84 bytes from 216.58.214.78 icmp_seq=1 ttl=112 time=30.641 ms
```

```
PC2> ping google.com
google.com resolved to 216.58.214.78

84 bytes from 216.58.214.78 icmp_seq=1 ttl=112 time=31.441 ms
```

```
admin> ping google.com
google.com resolved to 216.58.214.78

84 bytes from 216.58.214.78 icmp_seq=1 ttl=112 time=28.753 ms
```

```
[user@web1 ~]$ ping google.com

64 bytes from 216.58.214.78: icmp_seq=1 ttl=255 time=32.53 ms 
```


# V. Add a building

```
IOU2#sh running-config
interface Ethernet0/0
 switchport access vlan 10
 switchport trunk allowed vlan 10,20,30
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/0
 switchport access vlan 10
 switchport mode access
!

[...]

interface Ethernet2/0
 switchport trunk allowed vlan 10,20,30
 switchport trunk encapsulation dot1q
 switchport mode trunk
```
```
IOU1#sh running-config
interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 20
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 30
 switchport mode access

[...]

interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 ```

 ```
 IOU3#sh running-config
 interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
 ```

 ```
 R1#sh running-config
 interface FastEthernet0/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/0.10
 encapsulation dot1Q 10
 ip address 10.5.10.254 255.255.255.0
!
interface FastEthernet0/0.20
 encapsulation dot1Q 20
 ip address 10.5.20.254 255.255.255.0
!
interface FastEthernet0/0.30
 encapsulation dot1Q 30
 ip address 10.5.30.254 255.255.255.0
!
interface FastEthernet1/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
```

```
PC4> ping google.com   
google.com resolved to 216.58.214.78

84 bytes from 216.58.214.78 icmp_seq=1 ttl=112 time=32.038 ms

PC4> ping 1.1.1.1

84 bytes from 1.1.1.1 icmp_seq=4 ttl=255 time=19.064 ms   
84 bytes from 1.1.1.1 icmp_seq=5 ttl=255 time=21.682 ms 
```