
## I. ARP


| Machine  | `10.3.1.0/24` |
|----------|---------------|
| `John`   | `10.3.1.11`   |
| `Marcel` | `10.3.1.12`   |

### 1. Echange ARP

 - effectuer un `ping` d'une machine à l'autre

```
[user@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.53 ms
```
- observer les tables ARP des deux machines

John
```
[user@localhost ~]$ sudo ip neigh show dev enp0s8
10.3.1.12 lladdr 08:00:27:f9:97:33 STALE
```

Marcel
```
[user@localhost ~]$ sudo ip neigh show dev enp0s8
10.3.1.11 lladdr 08:00:27:63:f7:af STALE
```
- repérer l'adresse MAC de `John` dans la table ARP de `Marcel` et vice-versa

Adresse MAC de `John` : 08:00:27:63:f7:af

Adresse MAC de `Marcel` : 08:00:27:f9:97:33

- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la MAC de `Marcel` dans la table ARP de `John`
```
[user@localhost ~]$ sudo ip neigh show 10.3.1.12
```
  - et une commande pour afficher la MAC de `Marcel`, depuis `Marcel`

```
[user@localhost ~]$ ip link show

2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000ot found
    link/ether 08:00:27:f9:97:33 brd ff:ff:ff:ff:ff:ff
```

### 2. Analyse de trames


- utilisez la commande `tcpdump` pour réaliser une capture de trame

```
[user@localhost ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp3_arp.pcap not port 22
```
- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`

```
[user@localhost ~]$ sudo ip neigh flush all
```

**[TP3_ARP](Requin/arp.pcap)**

## II. Routage


| Machine  | `10.3.1.0/24` | `10.3.2.0/24` |
|----------|---------------|---------------|
| `router` | `10.3.1.254`  | `10.3.2.254`  |
| `John`   | `10.3.1.11`   | no            |
| `Marcel` | no            | `10.3.2.12`   |

### 1. Mise en place du routage

**Activer le routage sur le noeud `router`**

```
[user@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success

[user@localhost ~]$ sudo firewall-cmd --list-all
  masquerade: yes
```


- Ajout des routes

`John`
```
[user@localhost ~]$ ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8
```

`Marcel`
```
[user@localhost ~]$ ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8
```
- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre

```
[user@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=2.60 ms
```

```
[user@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=1.20 ms
```


### 2. Analyse de trames

**Analyse des échanges ARP**



| ordre | type trame  | IP source | MAC source                   | IP destination | MAC destination               |
|-------|-------------|-----------|------------------------------|----------------|-------------------------------|
| 1     | Requête ARP | x         | `router` `08:00:27:94:24:b6` | x              | Broadcast `ff:ff:ff:ff:ff:ff` |
| 2     | Réponse ARP | x         | `Marcel` `08:00:27:f9:97:33` | x              | `router` `08:00:27:94:24:b6`  |
| 3     | Ping        | 10.3.2.254| 08:00:27:94:24:b6            | 10.3.2.12      | 08:00:27:f9:97:33             |
| 4     | Pong        | 10.3.2.12 | 08:00:27:f9:97:33            | 10.3.2.254     | 08:00:27:94:24:b6             |
| 5     | Requête ARP | x         | `Marcel` `08:00:27:f9:97:33` | x              | `router` `08:00:27:94:24:b6`  |
| 6     | Réponse ARP | x         | `router` `08:00:27:94:24:b6` | x              | `Marcel` `08:00:27:f9:97:33`  |



**[TP3_ROUTAGE_Marcel](Requin/routage_Marcel.pcap)**

### 3. Accès internet

**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet

```
[user@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7d:ad:70 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84739sec preferred_lft 84739sec
    inet6 fe80::a00:27ff:fe7d:ad70/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000

4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
```

- ajoutez une route par défaut à `John` et `Marcel`

`John`
```
[user@localhost ~]$ ip route add default via 10.3.1.254 dev enp0s8
```

`Marcel`
```
[user@localhost ~]$ ip route add default via 10.3.2.254 dev enp0s8
```
  - vérifiez que vous avez accès internet avec un `ping`

`John`
```
[user@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=24.4 ms
```

`Marcel`
```
[user@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=23.9 ms
```

| ordre | type trame | IP source          | MAC source                 | IP destination     | MAC destination            |
|-------|------------|--------------------|----------------------------|--------------------|----------------------------|
| 1     | ping       | `John` `10.3.1.11` | `John` `08:00:27:63:f7:af` | `8.8.8.8`          | `08:00:27:27:f6:5a`        |
| 2     | pong       | `8.8.8.8`          | `08:00:27:27:f6:5a`        | `John` `10.3.1.11` | `John` `08:00:27:63:f7:af` |


**[TP3_ROUTAGE](Requin/routage_internet.pcapng)**

## III. DHCP


| Machine  | `10.3.1.0/24`              | `10.3.2.0/24` |
|----------|----------------------------|---------------|
| `router` | `10.3.1.254`               | `10.3.2.254`  |
| `John`   | `10.3.1.11`                | no            |
| `bob`    | oui mais pas d'IP statique | no            |
| `Marcel` | no                         | `10.3.2.12`   |

### 1. Mise en place du serveur DHCP


- installation du serveur sur `John`

```
[user@localhost ~]$ sudo journalctl -xeu dhcpd.service
Oct 11 18:13:13 localhost.localdomain systemd[1]: Starting DHCPv4 Server Daemon...
░░ Subject: A start job for unit dhcpd.service has begun execution
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit dhcpd.service has begun execution.
░░
░░ The job identifier is 1670.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Internet Systems Consortium DHCP Server 4.4.2b1
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Copyright 2004-2019 Internet Systems Consortium.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: All rights reserved.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: For info, please visit https://www.isc.org/software/dhcp/
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: ldap_gssapi_principal is not set,GSSAPI Authentication for LDAP will>
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Not searching LDAP since ldap-server, ldap-port and ldap-base-dn wer>
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Not searching LDAP since ldap-server, ldap-port and ldap-base-dn wer>
Oct 11 18:13:13 localhost.localdomain systemd[1]: Starting DHCPv4 Server Daemon...
░░ Subject: A start job for unit dhcpd.service has begun execution
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit dhcpd.service has begun execution.
░░
░░ The job identifier is 1670.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Internet Systems Consortium DHCP Server 4.4.2b1
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Copyright 2004-2019 Internet Systems Consortium.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: All rights reserved.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: For info, please visit https://www.isc.org/software/dhcp/
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: ldap_gssapi_principal is not set,GSSAPI Authentication for LDAP will>
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Not searching LDAP since ldap-server, ldap-port and ldap-base-dn wer>
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Config file: /etc/dhcp/dhcpd.conf
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Database file: /var/lib/dhcpd/dhcpd.leases
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: PID file: /var/run/dhcpd.pid
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Source compiled to use binary-leases
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Wrote 0 leases to leases file.
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Listening on LPF/enp0s8/08:00:27:63:f7:af/10.3.1.0/24
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Sending on   LPF/enp0s8/08:00:27:63:f7:af/10.3.1.0/24
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Sending on   Socket/fallback/fallback-net
Oct 11 18:13:13 localhost.localdomain dhcpd[1429]: Server starting service.
Oct 11 18:13:13 localhost.localdomain systemd[1]: Started DHCPv4 Server Daemon.
░░ Subject: A start job for unit dhcpd.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit dhcpd.service has finished successfully.
```
- créer une machine `bob`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

```
[user@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6a:05:b5 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.2/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
```

**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser

Ajout des lignes dans le fichier dhcpd.conf :
```
option routers 10.3.1.254;
option subnet-mask 255.255.255.0;
option domain-name-servers 8.8.8.8;
```
- récupérez de nouveau une IP en DHCP sur `bob` pour tester :

Pour libérer l'adresse IP actuelle
```
[user@localhost ~]$ dhclient -r :
```
Pour obtenir une nouvelle adresse IP :
```
[user@localhost ~]$ dhclient
```
  - `bob` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP

    ```
    [user@localhost ~]$ ip a
    2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:6a:05:b5 brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.2/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
          valid_lft 529sec preferred_lft 529sec
    ```
    - vérifier qu'il peut `ping` sa passerelle

    ```
    [user@localhost ~]$ ping 10.3.1.254
    PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
    64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.553 ms
    ```
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande

    ```
    [user@localhost ~]$ ip r s
    default via 10.3.1.254 dev enp0s8 proto dhcp src 10.3.1.2 metric 100
    10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.2 metric 100
    ```
    - vérifier que la route fonctionne avec un `ping` vers une IP

    ```
    [user@localhost ~]$ ping 10.3.2.12
    PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
    64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.24 ms
    ```
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne

    ```
    [user@localhost ~]$ dig google.com
    ;; ANSWER SECTION:
    google.com.             300     IN      A       216.58.209.238
    ```
    - vérifier un `ping` vers un nom de domaine

    ```
    [user@localhost ~]$ ping 216.58.209.238
    PING 216.58.209.238 (216.58.209.238) 56(84) bytes of data.
    64 bytes from 216.58.209.238: icmp_seq=1 ttl=247 time=25.4 ms
    ```

### 2. Analyse de trames

**Analyse de trames**


**[TP3_DHCP](Requin/dhcp.pcap)**
