# TP4 : TCP, UDP et services réseau


# I. First steps

Application 1 : ```Spotify```

```
[toto@fedora ~]$ ss -tunp

tcp    ESTAB      0       0                  10.33.16.2:39060      35.186.224.25:443   users:(("spotify",pid=11899,fd=32))  
```


**[Spotify](Requin/Spotify%20Requin.pcapng)**

Application 2 : ```Discord```

```
[toto@fedora ~]$ ss -tunp

tcp    ESTAB     0       0                  10.33.16.2:37322      35.186.224.47:443    users:(("Discord",pid=3148,fd=31)) 
```

**[Discord](Requin/Discord%20Requin.pcapng)**

Application 3 : ```Firefox```

```
[toto@fedora ~]$ ss -tunp

tcp    ESTAB     0       0                  10.33.16.2:55962     35.161.148.163:443    users:(("firefox",pid=5062,fd=126)) 
```

**[Firefox](Requin/Firefox%20Requin.pcapng)**


Application 4 : ```Discord```

```
[toto@fedora ~]$ ss -tunp

udp    ESTAB     0        0                    10.33.16.2:42551     162.159.134.233:443    users:(("Discord",pid=3148,fd=26)) 
```

**[Discord](Requin/Discord2%20Requin.pcapng)**


Application 5 : ```kioslave5```

```
[toto@fedora ~]$ ss -tunp

tcp     ESTAB   0        0                    10.33.16.2:57972       138.199.26.25:443    users:(("kioslave5",pid=23290,fd=6))        
```

**[kioslave5](Requin/Kioslave5%20Requin.pcapng)**


# II. Mise en place

## 1. SSH

**[3-way handshake](Requin/SSH%20Requin.pcapng)**

**[VM PAS D'ACCORD](Requin/SSH1%20Requin.pcapng)**

**[Fin de connexion](Requin/SSH2%20Requin.pcapng)**


```
ss -tnp
tcp    CLOSE-WAIT  1       0                  10.33.17.145:47532    51.144.164.215:22    users:(("code",pid=6682,fd=24)) 
```


# III. DNS

## 2. Setup

```bash
[user@dns-server ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /* 
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable 
           recursion. 
         - If your recursive DNS server has a public IP address, you MUST enable access 
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification 
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface 
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "tp4.b1" IN {
        type master;
        file "tp4.b1.db";
        allow-update { none; };
        allow-query {any; };
};

zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

```

```bash
[user@dns-server ~]$ sudo cat  /var/named/tp4.b1.db
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

@ IN NS dns-server.tp4.b1.

dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11
```

```bash
[user@dns-server ~]$ sudo cat  /var/named/tp4.b1.rev
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.
```
```bash
[user@dns-server ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Fri 2022-10-28 16:36:35 CEST; 14min ago
   Main PID: 1764 (named)
      Tasks: 5 (limit: 2684)
     Memory: 14.5M
        CPU: 33ms
     CGroup: /system.slice/named.service
             └─1764 /usr/sbin/named -u named -c /etc/named.conf
```
```
[user@dns-server ~]$ sudo ss -tpunl
Address:Port    Process                              
udp      UNCONN    0         0                 10.4.1.201:53                0.0.0.0:*        users:(("named",pid=1764,fd=19))
```

```
[user@dns-server ~]$ sudo firewall-cmd --list-all
[user@dns-server ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[user@dns-server ~]$ sudo firewall-cmd --reload
success
[user@dns-server ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 53/udp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

```

## 3. Test


```
[user@node1 ~]$ sudo cat  /etc/sysconfig/network-scripts/ifcfg-enp0s8 
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.4.1.11
NETMASK=255.255.255.0

GATEWAY=10.4.1.254
DNS1=10.4.1.201
```
```
[user@node1 ~]$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59836
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             199     IN      A       142.250.74.238

;; Query time: 30 msec
;; SERVER: 10.0.2.3#53(10.0.2.3)
;; WHEN: Fri Oct 28 17:23:08 CEST 2022
;; MSG SIZE  rcvd: 55

[user@node1 ~]$ dig dns-server.tp4.b1

; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 19984
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A

;; AUTHORITY SECTION:
.                       86369   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2022102800 1800 900 604800 86400

;; Query time: 59 msec
;; SERVER: 10.0.2.3#53(10.0.2.3)
;; WHEN: Fri Oct 28 17:23:36 CEST 2022
;; MSG SIZE  rcvd: 121
```


```
sudo cat  /etc/resolv.conf 
# This is /run/systemd/resolve/stub-resolv.conf managed by man:systemd-resolved(8).
# Do not edit.
#
# This file might be symlinked as /etc/resolv.conf. If you're looking at
# /etc/resolv.conf and seeing this text, you have followed the symlink.
#
# This is a dynamic resolv.conf file for connecting local clients to the
# internal DNS stub resolver of systemd-resolved. This file lists all
# configured search domains.
#
# Run "resolvectl status" to see details about the uplink DNS servers
# currently in use.
#
# Third party programs should typically not access this file directly, but only
# through the symlink at /etc/resolv.conf. To manage man:resolv.conf(5) in a
# different way, replace this symlink by a static file or a different symlink.
#
# See man:systemd-resolved.service(8) for details about the supported modes of
# operation for /etc/resolv.conf.
nameserver 10.4.1.201
nameserver 127.0.0.53
options edns0 trust-ad
search .

```

```
[toto@fedora ~]$ dig google.com

; <<>> DiG 9.16.33-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 51453
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ca96cf37918381e101000000635bf5315d3b4aae2aedb580 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.214.174

;; Query time: 29 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Fri Oct 28 17:28:49 CEST 2022
;; MSG SIZE  rcvd: 83

[toto@fedora ~]$ dig node1.tp4.b1

; <<>> DiG 9.16.33-RH <<>> node1.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56841
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ed083af6f908d92b01000000635bf53cff3c2d8439919194 (good)
;; QUESTION SECTION:
;node1.tp4.b1.                  IN      A

;; ANSWER SECTION:
node1.tp4.b1.           86400   IN      A       10.4.1.11

;; Query time: 8 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Fri Oct 28 17:29:00 CEST 2022
;; MSG SIZE  rcvd: 85
```

**[DNS (Requin Mignon)](Requin/Requin%20Mignon.pcapng)**
