# Exploration locale en solo

Affichez les infos des cartes réseau de votre PC

   commande à effectuer dans le terminal : ```ipconfig /all```
    
   ##     Interface WiFi

```
   Suffixe DNS propre à la connexion. . . :
      Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
      Adresse physique . . . . . . . . . . . : C4-BD-E5-79-F4-AA
      DHCP activé. . . . . . . . . . . . . . : Oui
      Configuration automatique activée. . . : Oui
      Adresse IPv6 de liaison locale. . . . .: fe80::7d31:b5b3:4b0f:ed0f%9(préféré)
      Adresse IPv4. . . . . . . . . . . . . .: 10.33.16.2(préféré)
      Masque de sous-réseau. . . . . . . . . : 255.255.252.0
      Bail obtenu. . . . . . . . . . . . . . : mercredi 5 octobre 2022 08:00:31
      Bail expirant. . . . . . . . . . . . . : jeudi 6 octobre 2022 08:00:31
      Passerelle par défaut. . . . . . . . . : 10.33.19.254
      Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
      IAID DHCPv6 . . . . . . . . . . . : 113556965
      DUID de client DHCPv6. . . . . . . . : 00-01-00-01-29-A5-B3-15-88-A4-C2-9C-99-84
      Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                          8.8.4.4
                                          1.1.1.1
      NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

   Nom : ```Intel(R) Wi-Fi 6 AX201 160MHz```
      
   Adresse MAC : ```C4-BD-E5-79-F4-AA```
      
   IP : ```10.33.16.2```


   ##    Interface Ethernet

   ```
      Statut du média. . . . . . . . . . . . : Média déconnecté
      Suffixe DNS propre à la connexion. . . :
      Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
      Adresse physique . . . . . . . . . . . : 88-A4-C2-9C-99-84
      DHCP activé. . . . . . . . . . . . . . : Oui
      Configuration automatique activée. . . : Oui
   ```
      
Nom : ```Realtek PCIe GbE Family Controller```


Adresse MAC : ```88-A4-C2-9C-99-84```
   
   
###    Afficher son gateway
   
   Adresse IP de la passerelle (gateway) de la carte WIFI : ```10.33.19.254```
   
   
   ##    Déterminer la MAC de la passerelle

   Commande à effectuer dans le terminal : ```arp -a```

   ```
      Interface : 192.168.71.1 --- 0x5
      Adresse Internet      Adresse physique      Type
      192.168.71.254        00-50-56-ee-ae-30     dynamique
      192.168.71.255        ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
      255.255.255.255       ff-ff-ff-ff-ff-ff     statique

      Interface : 10.33.16.2 --- 0x9
      Adresse Internet      Adresse physique      Type
      10.33.19.254          00-c0-e7-e0-04-4e     dynamique
      10.33.19.255          ff-ff-ff-ff-ff-ff     statique
      224.0.0.2             01-00-5e-00-00-02     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
      255.255.255.255       ff-ff-ff-ff-ff-ff     statique

      Interface : 192.168.5.1 --- 0xb
      Adresse Internet      Adresse physique      Type
      192.168.5.254         00-50-56-ef-c9-2e     dynamique
      192.168.5.255         ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
      255.255.255.255       ff-ff-ff-ff-ff-ff     statique
   ```

 Adresse MAC de la passerelle : ```00-c0-e7-e0-04-4e```


## En graphique (GUI : Graphical User Interface)

#### Chemin d'accès :
   ##### Informations système > Composant > Réseau > Carte

   ```
    Adresse IP	10.33.16.2, fe80::7d31:b5b3:4b0f:ed0f
    Adresse MAC	‪C4:BD:E5:79:F4:AA‬
    Passerelle IP par défaut	10.33.19.254
```

## Modifications des informations

### Modification d'adresse IP

#### Chemin d'accès :
   ##### Panneau de configuration > Réseau et Internet > Centre Réseau et partage > Modifier les paramètres de la carte > Wi-Fi > Etat de Wi-Fi > Propriétés de Wi-Fi > Protocole Internet version 4 > Utiliser l'adresse IP suivante

   Essais pour IP : ```10.33.16.241```
      (Masque de sous-réseau et gateway similaires à ceux relevés)

      => Pas de perte d'accès à internet.

 Il est possible de perdre l'accès si une autre personne utilisait déjà cette adresse IP.


# Exploration locale en duo

   ### Modification de l'IP
   ```
      IP : 10.10.10.2
      Masque de sous-réseau : 255.255.255.0
   ```

   #### Vérifier à l'aide d'une commande que votre IP a bien été changée

   ```ipconfig```

      => On voit bien le changement

   #### Vérifier que les deux machines se joignent

   ```ping```
         
 *(Bien penser au firewall)*

 ```Envoi d’une requête 'Ping'  10.10.10.2 avec 32  octets de données :
    Réponse de 10.10.10.2 : octets=32 temps=1 ms TTL=64
    Réponse de 10.10.10.2 : octets=32 temps=1 ms TTL=64
    Réponse de 10.10.10.2 : octets=32 temps=1 ms TTL=64
    Réponse de 10.10.10.2 : octets=32 temps=1 ms TTL=64`
```
#### Déterminer l'adresse MAC de votre correspondant

```apr -a```

Adresse MAC du correspondant : ```88-a4-c2-ac-a8-2b```


## Utilisation d'un des deux comme gateway

Coupure d'internet sur ma machine. Mise de la passerelle de l'autre machine.

### Tester l'accès internet
```ping 1.1.1.1```

```
Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=24 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=34 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=46 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=24 ms TTL=54
```

### Prouver que la connexion Internet passe bien par l'autre PC

```tracert 1.1.1.1```

```
Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :

  1    <1 ms    <1 ms    <1 ms  10.10.10.2
  2     8 ms     3 ms     4 ms  10.33.19.254
  3    13 ms     6 ms     4 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
```
On retrouve bien l'IP du PC donnant l'accès

## Petit chat privé

( Installation de netcat sur windows avec Edge => UN ENFER ! )

Depuis netcat :

```
Cmd line: 10.10.10.2 8888
g
coucou
Bonsoir !
Miaou
```

### Visualiser la connexion en cours
```
TCP    10.10.10.1:63681       10.10.10.2:8888        ESTABLISHED
[nc64.exe]
```

(Voir TP Killian)

## Firewall

Autoriser les Pings(s) :
![Autoriser les Pings(s)](screens/Ping.png)


Discuter sur netcat malgré le pare-feu actif :

![Autoriser netcat sur 8888](screens/netcat.png)


```
Cmd line: 10.10.10.2 8888
Bonsoir !
! riosnoB
```

# Manipulations d'autres outils/protocoles côté client

## Exploration du DHCP, depuis votre PC

```ipconfig /all```
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : C4-BD-E5-79-F4-AA
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6. . . . . . . . . . . . . .: 2a01:cb19:7a6:4e00:7d31:b5b3:4b0f:ed0f(préféré)
   Adresse IPv6 temporaire . . . . . . . .: 2a01:cb19:7a6:4e00:34cb:7b11:94d6:b15a(préféré)
   Adresse IPv6 de liaison locale. . . . .: fe80::7d31:b5b3:4b0f:ed0f%9(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.20(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : mercredi 5 octobre 2022 14:55:45
   Bail expirant. . . . . . . . . . . . . : jeudi 6 octobre 2022 14:55:44
   Passerelle par défaut. . . . . . . . . : fe80::a21b:29ff:fec3:875c%9
                                       192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 113556965
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-29-A5-B3-15-88-A4-C2-9C-99-84
   Serveurs DNS. . .  . . . . . . . . . . : fe80::a21b:29ff:fec3:875c%9
                                       192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   Liste de recherche de suffixes DNS propres à la connexion :
                                       home
```
*Fait depuis chez moi*
Le bail a été obtenue  `mercredi 5 octobre 2022 `à `14:55:45` et semble valable `23:59:59`

## DNS 
```ipconfig /all```
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : C4-BD-E5-79-F4-AA
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6. . . . . . . . . . . . . .: 2a01:cb19:7a6:4e00:7d31:b5b3:4b0f:ed0f(préféré)
   Adresse IPv6 temporaire . . . . . . . .: 2a01:cb19:7a6:4e00:34cb:7b11:94d6:b15a(préféré)
   Adresse IPv6 de liaison locale. . . . .: fe80::7d31:b5b3:4b0f:ed0f%9(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.20(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : mercredi 5 octobre 2022 14:55:45
   Bail expirant. . . . . . . . . . . . . : jeudi 6 octobre 2022 14:55:44
   Passerelle par défaut. . . . . . . . . : fe80::a21b:29ff:fec3:875c%9
                                       192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 113556965
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-29-A5-B3-15-88-A4-C2-9C-99-84
   Serveurs DNS. . .  . . . . . . . . . . : fe80::a21b:29ff:fec3:875c%9
                                       192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   Liste de recherche de suffixes DNS propres à la connexion :
                                       home
```
*Fait depuis chez moi*

Serveur DNS : `192.168.1.1`

```nslookup google.com```
```
Serveur :   UnKnown
Address:  fe80::a21b:29ff:fec3:875c

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:805::200e
          216.58.204.110
````
IP du serveur `google.com` : 
```216.58.204.110```

```nslookup ynov.com```
```
Serveur :   UnKnown
Address:  fe80::a21b:29ff:fec3:875c

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:ae9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          172.67.74.226
          104.26.10.233
          104.26.11.233
````
IP(s) du serveur `ynov.com` : 
```172.67.74.226``` ;
 `104.26.10.233` ;
  `104.26.11.233`

```nslookup 231.34.113.12```
```
Serveur :   UnKnown
Address:  fe80::a21b:29ff:fec3:875c

*** UnKnown ne parvient pas à trouver 231.34.113.12 : Non-existent domain
```
```nslookup 78.34.2.17```
```
Serveur :   UnKnown
Address:  fe80::a21b:29ff:fec3:875c

Nom :    cable-78-34-2-17.nc.de
Address:  78.34.2.17
```

# Wireshark
Cliquer sur Ethernet => Unsaved packets => continuer sans enregistrer
Ping entre moi, et ma passerelle (pc du mate ; ici Killian)
![Ping Wireshark](screens/PingIV.png)

Netcat entre nous :
![Wireshark netcat](screens/netcatIV.png)


Requête DNS :
![Requête DNS](screens/DNS1.png)

Réponse :
![Réponse](screens/DN2.png)

## Bonus : avant-goût TCP et UDP

### Wireshark it
Déterminer à quelle IP et quel port le PC se connecte quand on regarde une vidéo Youtube...
![Youtube Wireshark](screens/Video.png)