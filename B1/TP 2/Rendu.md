# Setup IP

Choix des IP
IP privés choisis en 10. [...] car on ne cherche qu'une amplitudes de 1000 IP(s).
Le masque adapté sera /22 (car 2^8 = 1024 ; puissance de 2 la plus proche de et suprérieur à 1000).
```
Network address : 10.24.16.0
IP (1) : `10.24.16.3/22`
IP (2) : `10.24.19.254/22`
Broadcast address : `10.24.16.255`
```
Changement d'IP :
```
netsh interface ip show config
```

```
Configuration pour l'interface « Ethernet »
    DHCP activé :                         Non
    Adresse IP :                           169.254.20.210
    Préfixe de sous-réseau :               169.254.0.0/16 (masque 255.255.0.0)
    Métrique de l'interface :             25
    Serveurs DNS configurés statiquement : 8.8.8.8
    Enregistrer avec le suffixe :           Principale uniquement
    Serveurs WINS configurés statiquement : Aucun
```
(Dans ce cas, il faut activer le DHCP)

```
netsh interface ip set address “Ethernet” dhcp
```
On peut désormais changer l'IP

```
netsh interface ip set address name= “Ethernet” static 10.24.16.3 255.255.252.0
```
On vérifie que cela fontionne...

```
ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::8025:6bfb:a6e1:14d2%5
   Adresse IPv4. . . . . . . . . . . . . .: 10.24.16.3
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . :
```

On est joie !

C'est donc l'heure de ping décisif

```
PC(1) :
ping 10.24.19.254
```

```
PC(2) :
ping 10.24.16.3
```

Voir [Ping(&Pong)](./Requin/Ping(%26Pong).pcapng)

# ARP my bro

```
arp -a

Interface : 10.24.16.3 --- 0x5
  Adresse Internet      Adresse physique      Type
  10.24.19.254          00-26-b9-11-ed-dc     dynamique
  10.24.19.255          ff-ff-ff-ff-ff-ff     statique
  10.33.19.254          00-26-b9-11-ed-dc     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
MAC de IP(2) : ```00-26-b9-11-ed-dc```

MAC de IP(1) (foruni par PC 2) : ```88-a4-c2-9c-99-84```

MAC WIFI (Ynov) : ```00-c0-e7-e0-04-4e```

Vider la table arp :

```
netsh interface IP delete arpcache
```
```
arp -a

Interface : 10.24.16.3 --- 0x5
  Adresse Internet      Adresse physique      Type
  10.24.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
```
```
ping 10.24.19.254
```

```
arp -a

Interface : 10.24.16.3 --- 0x5
  Adresse Internet      Adresse physique      Type
  10.24.19.254          00-26-b9-11-ed-dc     dynamique
  10.24.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Now, It's TEA TIME !
(La pause thé aura durée 3 jours)

Requête ARP :

Voir [Meeting](./Requin/Meeting.pcapng)

Première tram ARP : Mon adresse MAC `88:a4:c2:9c:99:84` envoi à l'adresse de broadcast `ff:ff:ff:ff:ff:ff` un message pour demander la MAC de l'IP `10.24.19.254`.

La seconde tram est la réponse de l'IP `10.24.19.254` qui reponds de manière sympathique : "Salut, mon adresse MAC c'est `00:26:b9:11:ed:dc`.

(Les deux tram(s) suivantes sont les échanges ARP initiés par ma collègue.)

# Interlude hackerzz

Done

# DHCP you too my brooo

```
netsh interface ip set address name= “Wi-fi” static 99.24.16.3 255.255.252.0
```
```
netsh interface ip set address name= “Wi-fi” dhcp
```
(Ici, nous avons du utiliser l'interface graphique pour sélectionner le réseau Ynov)

Voir [DORA](./Requin/Sans_Papiers.pcapng)

Tram 1 (Discover) : La source est une adresse MAC: `c4:bd:e5:79:f4:aa` à destination de la MAC de broadcast : `ff:ff:ff:ff:ff:ff`.

Tram 2 (Offer) : La source est la MAC : `00:c0:e7:e0:04:4e` qui propose à ma MAC (`c4:bd:e5:79:f4:aa`) les informations utiles.
```
Your (client) IP address: 10.33.16.2
DHCP Server Identifier: 10.33.19.254
Domain Name Server: 8.8.8.8
Domain Name Server: 8.8.4.4
Domain Name Server: 1.1.1.1
```
Tram 3 (Request) : Mon adresse MAC envoit à l'adresse MAC de Broadcast que c'est d'accord pour cette IP.

Tram 4 (ACK) : La passerelle valide la transaction ; L'IP : `10.33.16.2` m'est attribuée.

Informations utiles (de la trame offer) :
- L'IP à utiliser : `10.33.16.2`
- L'IP de la passerelle de réseau : `10.33.19.254`
- Serveur(s) DNS : `8.8.8.8` ; `8.8.4.4` ;  `1.1.1.1`