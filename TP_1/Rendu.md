# TP1 : Maîtrise réseau du poste

## I. Basics

☀️ **Carte réseau WiFi**

```bash
(base)  ✘ toto@fedora37  ~   ip a | tail -n 6
3: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether c4:bd:e5:79:f4:aa brd ff:ff:ff:ff:ff:ff
    inet 192.168.202.125/24 brd 192.168.202.255 scope global dynamic noprefixroute wlp0s20f3
       valid_lft 3033sec preferred_lft 3033sec
    inet6 fe80::b68b:db10:a825:6de/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

```

- MAC de la carte WiFi : `c4:bd:e5:79:f4:aa`
- IP de la carte WiFi : `192.168.202.125`
- Masque de sous-réseau du réseau LAN : `/24` |`(255.255.255.0)`

☀️ **Déso pas déso**

- Adresse du réseau du LAN : `192.168.202.0`
- Adresse de broadcast : `192.168.202.255`
- Nombre d'adresses IP disponibles : `253`

☀️ **Hostname**

```bash
(base)  ✘ toto@fedora37  ~   hostname
fedora37

```
> A changer, je suis passé au 38 xD*

☀️ **Passerelle du réseau**

```bash
(base)  toto@fedora37  ~  ip r s
default via 10.33.79.254 dev wlp0s20f3 proto dhcp src 10.33.70.3 metric 600 

```
> refait sur la co d'Ynov

- Adresse IP de la passerelle : `192.168.202.140` (ici mon smartphone)
- Adresse MAC de la passerelle : `06:2f:71:81:4d:49`

☀️ **Serveur DHCP et DNS**

```bash
(base)  ✘ toto@fedora37  ~    nmcli con show "Nom du réseau" | grep -i dhcp | grep dhcp_server_identifier

DHCP4.OPTION[4]:                        dhcp_server_identifier = 192.168.202.140

(base)  ✘ toto@fedora37  ~   resolvectl | tail -n 6
Link 3 (wlp0s20f3)
    Current Scopes: DNS LLMNR/IPv4 LLMNR/IPv6
         Protocols: +DefaultRoute LLMNR=resolve -mDNS -DNSOverTLS
                    DNSSEC=no/unsupported
Current DNS Server: 192.168.202.140
       DNS Servers: 192.168.202.140 2a02:8440:6340:93ee::52

```

- Adresse IP du serveur DHCP : `192.168.202.140`
- Adresse IP du serveur DNS : `192.168.202.140`

☀️ **Table de routage**

```bash
(base)  ✘ toto@fedora37  ~   ip r s | grep default
default via 192.168.202.140 dev wlp0s20f3 proto dhcp src 192.168.202.125 metric 600 

```

- Route par défaut : `192.168.202.140`


## II. Go further

☀️ **Hosts ?**

```bash
(base)  ✘ toto@fedora37  ~   sudo vim /etc/hosts
(base)  ✘ toto@fedora37  ~  cat /etc/hosts | tail -n 3
#10.105.1.11  web.tp5.linux
#10.105.1.13 www.nextcloud.tp5
1.1.1.1 b2.hello.vous

```

> Oui il y a un peut clin d'oeil

```bash
(base)  ✘ toto@fedora37  ~  ping b2.hello.vous
PING b2.hello.vous (1.1.1.1) 56(84) octets de données.
64 octets de b2.hello.vous (1.1.1.1) : icmp_seq=1 ttl=55 temps=39.4 ms
64 octets de b2.hello.vous (1.1.1.1) : icmp_seq=2 ttl=55 temps=62.0 ms
^C

```

☀️ **Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...**

```bash
ESTAB      0      0      192.168.202.125:38186  34.117.65.55:443  users:(("firefox",pid=3957,fd=189)) 

```
- Adresse IP du serveur : `34.117.65.55`
- Port du serveur : `443`
- Port local : `38186`

☀️ **Requêtes DNS**

```bash
(base)  ✘ toto@fedora37  ~   ping -4  www.ynov.com
PING  (104.26.11.233) 56(84) octets de données.
64 octets de 104.26.11.233 (104.26.11.233) : icmp_seq=1 ttl=55 temps=143 ms
64 octets de 104.26.11.233 (104.26.11.233) : icmp_seq=2 ttl=55 temps=44.4 ms
^C

(base)  ✘ toto@fedora37  ~   dig -x 174.43.238.89 | grep .com
89.238.43.174.in-addr.arpa. 7128 IN	PTR	89.sub-174-43-238.myvzw.com.

```

- Adresse IP du domaine `www.ynov.com` : `104.26.11.233`
- Nom de domaine de `174.43.238.89` : `89.sub-174-43-238.myvzw.com`

☀️ **Hop hop hop**

```bash
(base)  toto@fedora37  ~  traceroute www.ynov.com | tail -n 1
10  104.26.10.233 (104.26.10.233)  79.461 ms  79.444 ms  78.892 ms

```

- Les paquets passent par `10 machines` pour joindre `www.ynov.com`


> Retour sur la co de Ynov (grâce à un détour en 208)


☀️ **IP publique**

```bash
(base)  toto@fedora37  ~  curl -s ifconfig.me
195.7.117.146

```

☀️ **Scan réseau**

```bash
(base)  toto@fedora37  ~  nmap -sn 10.33.68.0/20   
Starting Nmap 7.93 ( https://nmap.org ) at 2023-10-19 08:56 CEST

```

> Scan arp c'est mieux (nmap -PR)

![CLong](/pics/wait.jpg)

```bash
[...]

Nmap scan report for 10.33.79.193
Host is up (0.078s latency).
Nmap done: 4096 IP addresses (199 hosts up) scanned in 367.43 seconds

```

# III. Le requin

☀️ **Capture ARP**

Filtre : `arp`
[Lien vers la capture ARP](/requin/arp.pcap)

☀️ **Capture DNS**

```bash
(base)  toto@fedora37  ~  drill example.com

```

Filtre : `dns`

[Lien vers la capture DNS](/requin/dns.pcap)

☀️ **Capture TCP**

Filtre : `tcp.stream eq 0`

[Lien vers la capture TCP](/requin/tcp.pcap)