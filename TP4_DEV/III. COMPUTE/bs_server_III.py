import argparse
import logging
import time
import socket
import threading
import logging

class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

runnin = True
def timecount():
    current_time = time.time()
    while runnin :
        if time.time() - current_time >= 60:
            logger.warning("Aucun client depuis plus de une minute.")
            print("Aucun client depuis plus de une minute.")
            current_time = time.time()

time_thread = threading.Thread(target=timecount)

parser = argparse.ArgumentParser(description="bs_server_III")

logger = logging.getLogger('server_logger')
logger.setLevel(logging.INFO)

# create logger with 'spam_application'
logger = logging.getLogger("bs_server_III")
logger.setLevel(logging.DEBUG)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

ch.setFormatter(CustomFormatter())

logger.addHandler(ch)
file_handler = logging.FileHandler('/var/log/bs_server/bs_server.log')

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

parser.add_argument("-p", "--port", type=int, help="Port to listen on", default=13337)
# parser.add_argument("-h", "--help")

args = parser.parse_args()

if args.port < 0 or args.port > 65535:
    print("ERROR: The specified port is not a valid port (0-65535).")
    exit(1)
elif 0 <= args.port <= 1024:
    print("ERROR: The specified port is a privileged port. Please use a port above 1024.")
    exit(2)

# RAISE IS BETTER for errors

host = '10.1.1.12'
port = args.port

logger.info("Lancement du serveur")
last_client_time = time.time()
logger.info("Le serveur tourne sur {}:{}".format(host, port))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port)) 

time_thread.start()

 
while True:

    s.listen(1)
    conn, addr = s.accept()
    if conn :
        runnin = not runnin
    logger.info("Un client {} s'est connecté.".format(addr))
    print(f"Un client vient de se co et son IP c'est {addr}.")

    try:
        data = conn.recv(1024).decode('utf-8')
        
        if not data: break
        logger.info("Le client {} a envoyé {}".format(addr, data))
        print(f"Données reçues du client : {data}")

        message = "Will be send to the client"
        message = eval(data)
        logger.info("Réponse envoyée au client {} : {}".format(addr, message))
        conn.sendall(str(message).encode('utf-8'))
    except socket.error:
        logger.error(f'Error Occured. ', {socket.error})
        print("Error Occured.")
        break
logger.info("Le serveur s'est arreté correctement")
conn.close()
runnin = False
