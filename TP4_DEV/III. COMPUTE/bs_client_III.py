import logging
import socket
import re

logger = logging.getLogger('client_logger')
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler('/var/log/bs_client/bs_client.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

logger_errors = logging.getLogger('bs_client_errors')
logger_errors.setLevel(logging.ERROR)

console_handler = logging.StreamHandler()
console_formatter = logging.Formatter('\033[91mERROR %(message)s\033[0m')
console_handler.setFormatter(console_formatter)
logger_errors.addHandler(console_handler)

 
host = '10.1.1.12'
port = 13337

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host, port))
    print(f"Connecté avec succès au serveur {host} sur le port {port}")
    logger.info("Connexion réussie à {}:{}".format(host, port))
except :
    print("Sa marche pô, déso.")
    logger.error("Impossible de se connecter au serveur {} : {}.".format(host, port))
    logger_errors.error("Impossible de se connecter au serveur {} : {}.".format(host, port))
    exit(2)

message = input("Que veux-tu calculer ? : ")
if type(message) is not str :
    s.close()
    raise TypeError("Dialecte primaire non compris.")

message = message.replace(" ","")
        
if not re.match(r"^[-+]?(100000|\d{1,5})(,[0-9]{3})*([+*-]([-+]?(100000|\d{1,5})(,[0-9]{3})*)+)*$", message):
    s.close()
    raise ValueError("Ounga Ounga ?")

# C'est vraiment au client de regarder si l'envoi a une bonne forme ?

s.sendall(message.encode('utf-8'))
logger.info("Message envoyé au serveur {}:{}".format(host, message))


data = s.recv(1024).decode('utf-8')
logger.info("Réponse reçue du serveur {}:{}".format(host, data))
print(f"Le serveur a répondu {repr(data)}")
s.close()


# Ajouter plus de logs ?
