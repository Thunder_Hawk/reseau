import socket

host = '10.1.1.12'
port = 13337

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((host, port))

s.sendall('Meooooo !'.encode('utf-8'))

data = s.recv(1024).decode('utf-8')

s.close()

print(f"Le serveur a répondu {repr(data)}")
