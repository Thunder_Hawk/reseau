import socket

host = '10.1.1.12'
port = 13337

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host, port))
    print(f"Connecté avec succès au serveur {host} sur le port {port}")
except TypeError:
    print("Sa marche pô, déso.")

message = input("Que veux-tu envoyer au serveur : ")
s.sendall(message.encode())

data = s.recv(1024).decode('utf-8')

s.close()

print(f"Le serveur a répondu {repr(data)}")
