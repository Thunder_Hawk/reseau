# TP4 : I'm Socketing, r u soketin ?

## I. Simple bs program

### 1. First steps


 #### 🌞 Commandes


 ```bash
[user@server ~]$ sudo firewall-cmd --add-port=13337/tcp --permanent
success
[user@server ~]$ sudo firewall-cmd --reload
success
[user@server ~]$ python bs_server_I1.py

[user@server ~]$ sudo ss -altnp4 | grep python
LISTEN 0      1          10.1.1.12:13337      0.0.0.0:*    users:(("python",pid=871,fd=4))
 ```
> A noter que certaines commandes, comme le clone de répo et l'inst ll  tion de packets ont été faits dans le TP précédent.


## II. You say dev I say good practices

### 2. Logs

#### A. Logs serveur

```bash
[user@server ~]$ sudo mkdir /var/log/bs_server -m 700 && sudo chown -R $(whoami):$(whoami) /var/log/bs_server && touch /var/log/bs_server/bs_server.log && chmod 600 /var/log/bs_server/bs_server.log

```

#### B. Logs client

```bash
[user@client ~]$ sudo mkdir /var/log/bs_client -m 700 && sudo chown -R $(whoami):$(whoami) /var/log/bs_client && touch /var/log/bs_client/bs_client.log && chmod 600 /var/log/bs_client/bs_client.log

```


