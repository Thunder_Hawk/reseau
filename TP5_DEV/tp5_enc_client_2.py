import socket


def binaire(msg):
    return msg.to_bytes((msg.bit_length() + 7) // 8, byteorder='big')


def unbinaire(msg):
    return int.from_bytes(msg, byteorder='big')


def send(lst, signe):
    header_int = len(binaire(lst[0])).to_bytes(2, byteorder='big')
    header = len(header_int + binaire(lst[0]) + signe.encode() + binaire(lst[1])).to_bytes(2, byteorder='big')
    seq_fin = "<clafin>".encode()
    return header + header_int + binaire(lst[0]) + signe.encode() + binaire(lst[1]) + seq_fin


def receive(skt):
    data = skt.recv(2)
    if data == b"":
        return
    msg_len = int.from_bytes(data[0:2], byteorder='big')
    chunks = []
    bytes_received = 0
    while bytes_received < msg_len:
        chunk = skt.recv(min(msg_len - bytes_received,
                                1024))
        if not chunk:
            raise RuntimeError('Invalid chunk received bro')
        chunks.append(chunk)
        bytes_received += len(chunk)
    fin = skt.recv(8)
    if fin.decode() != "<clafin>":
        raise RuntimeError('Invalid chunk received bro')
    else:
        return b"".join(chunks)


message = input("I will do it for you: ")

if type(message) is not str :
    raise TypeError("Fréro une string stp, c'est pourtant pas compliqué !")
    
message = message.replace(" ", "")

operator = "+" if "+" in message else ("-" if "-" in message else ("*" if "*" in message else False))

if not operator :
    raise ValueError("No valid operator found")

message_values = message.split(operator)
values = [0, 0]
if len(message_values) != 2:
    raise ValueError('"Super" calculator can manipulate only 2 values.')
try:
    values[0] = int(message_values[0])
    values[1] = int(message_values[1])
except ValueError as e:
    raise ValueError("Error occured trying to cast values as integers.")
for value in values:
    if len(binaire(value)) > 4:
        raise ValueError("Overflow")


header = len(message.encode()).to_bytes(4, byteorder='big')

clafin = "<clafin>".encode()

if operator in message :
    msg_lst = message.split(operator)
    lst = [0, 0]
    if len(msg_lst) != 2:
        print("Input not correct")
        exit(1)
    try:
        lst[0] = int(msg_lst[0])
        lst[1] = int(msg_lst[1])
    except ValueError:
        print("Value Error")
        exit(1)
    for i in lst:
        if len(binaire(i)) > 4:
            print("Overflow")
            exit(1)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 9999))

    s.send(send(lst, operator))

else:
    exit(1)

s_data = unbinaire(receive(s))
print(f"Easy : {s_data}")
s.close()