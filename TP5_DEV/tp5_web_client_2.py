import socket

def send_request():
    host = 'localhost'
    port = 8000

    request = "GET / HTTP/1.1\r\nHost: {0}\r\n\r\n".format(host)

    with socket.create_connection((host, port)) as client_socket:
        client_socket.sendall(request.encode('utf-8'))

        response = b""
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            response += data

    print(response.decode('utf-8'))

if __name__ == "__main__":
    send_request()