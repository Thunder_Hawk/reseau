import socket


def binaire(message):
    return message.to_bytes((message.bit_length() + 7) // 8, byteorder='big')


message = input("I will do it for you: ")

if type(message) is not str :
    raise TypeError("Fréro une string stp, c'est pourtant pas compliqué !")
    
message = message.replace(" ", "")

operator = "+" if "+" in message else ("-" if "-" in message else ("*" if "*" in message else False))

if not operator :
    raise ValueError("No valid operator found")

message_values = message.split(operator)
values = [0, 0]
if len(message_values) != 2:
    raise ValueError('"Super" calculator can manipulate only 2 values.')
try:
    values[0] = int(message_values[0])
    values[1] = int(message_values[1])
except ValueError as e:
    raise ValueError("Error occured trying to cast values as integers.")
for value in values:
    if len(binaire(value)) > 4:
        raise ValueError("Overflow")


header = len(message.encode()).to_bytes(4, byteorder='big')

clafin = "<clafin>".encode()

message = header + message.encode() + clafin

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('10.1.1.12', 13337))

s.send(message)

s_data = s.recv(1024)
print(s_data.decode())
s.close()