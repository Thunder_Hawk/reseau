import socket

def handle_request(client_socket):
    request_data = client_socket.recv(1024).decode('utf-8')
    
    request_line = request_data.split('\n')[0]
    method, path, protocol = request_line.split()

    response_content = "<h1>Hello, je suis un serveur HTTP</h1>"
    response = f"HTTP/1.1 200 OK\n\n{response_content}"

    client_socket.send(response.encode('utf-8'))

def run_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('localhost', 8000))
    server_socket.listen(1)
    print('Listening on port 8000...')

    while True:
        client_socket, client_address = server_socket.accept()
        print(f"Accepted connection from {client_address}")
        handle_request(client_socket)
        client_socket.close()

if __name__ == "__main__":
    run_server()