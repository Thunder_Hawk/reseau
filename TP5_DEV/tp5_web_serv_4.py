import socket
import logging
from datetime import datetime

logging.basicConfig(filename='server.log', level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

def handle_request(client_socket, request_path):
    try:
        if request_path == "/":
            request_path = "/toto.html"
        file_path = 'html' + request_path
        with open(file_path, 'r') as file:
            html_content = file.read()

        http_response = 'HTTP/1.1 200 OK\n\n' + html_content
        client_socket.sendall(http_response.encode('utf-8'))

        log_message = f'{client_socket.getpeername()[0]} - - [{datetime.now().strftime("%d/%b/%Y %H:%M:%S")}] "GET {request_path}" 200 -'
        logging.info(log_message)
    except FileNotFoundError:
        not_found_response = 'HTTP/1.1 404 Not Found\n\n404 Not Found'
        client_socket.sendall(not_found_response.encode('utf-8'))

        log_message = f'{client_socket.getpeername()[0]} - - [{datetime.now().strftime("%d/%b/%Y %H:%M:%S")}] "GET {request_path}" 404 -'
        logging.info(log_message)

def run_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('localhost', 8000))
    server_socket.listen(1)
    print('Listening on port 8000...')

    while True:
        client_socket, client_address = server_socket.accept()
        print(f"Accepted connection from {client_address}")

        request_data = client_socket.recv(1024).decode('utf-8')
        request_path = request_data.split(' ')[1]

        print(f"Request for: {request_path}")

        handle_request(client_socket, request_path)
        client_socket.close()

if __name__ == "__main__":
    logging.info(f"{datetime.now().strftime("%d/%b/%Y %H:%M:%S")} Server stated ")
    run_server()