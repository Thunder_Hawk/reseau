import asyncio

CLIENTS = {}

async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    print(f"Client connected from {client_address}")

    addr = client_address
    if addr not in CLIENTS:
        data = await reader.read(1024)
        message = data.decode()
        if message.startswith("Hello|"):
            pseudo = message.split("|")[1]
            print(f"{pseudo} has joined the chatroom!")
            CLIENTS[addr] = {"r": reader, "w": writer, "pseudo": pseudo}

            for client_addr, client_info in CLIENTS.items():
                if client_addr != addr:
                    client_writer = client_info["w"]
                    client_writer.write(f"Annonce: {pseudo} a rejoint la chatroom".encode())
                    await client_writer.drain()

    try:
        while True:
            data = await reader.read(1024)
            if not data:
                break

            message = data.decode()

            if message == "":
                break

            for client_addr, client_info in CLIENTS.items():
                if client_addr != addr:
                    client_pseudo = client_info["pseudo"]
                    client_writer = client_info["w"]
                    client_writer.write(f"{client_pseudo} a dit : {message}".encode())
                    await client_writer.drain()

    except asyncio.CancelledError:
        pass

    finally:
        if addr in CLIENTS:
            pseudo = CLIENTS[addr]["pseudo"]
            print(f"{pseudo} has left the chatroom!")

            for client_addr, client_info in CLIENTS.items():
                client_writer = client_info["w"]
                client_writer.write(f"Annonce: {pseudo} a quitté la chatroom".encode())
                await client_writer.drain()

            del CLIENTS[addr]
            writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '127.0.0.1', 8888
    )

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())