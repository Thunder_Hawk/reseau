import aiofiles
import asyncio
import sys
import time

from web_async import get_content, write_content

async def process_url(url):
    try:
        content = await get_content(url)
        file_name = f"/tmp/web_{url.replace('https://', '').replace('/', '_')}"
        await write_content(content, file_name)
        print(f"Content downloaded from {url} and saved to {file_name}")
    except Exception as e:
        print(f"An error occurred for {url}: {e}")

async def main():
    if len(sys.argv) != 2:
        print("Usage: python script.py <URLs_file>")
        return

    urls_file = sys.argv[1]
    try:
        async with aiofiles.open(urls_file, 'r') as file:
            urls = [url.strip() for url in await file.readlines() if url.strip()]

        tasks = [process_url(url) for url in urls]
        await asyncio.gather(*tasks)

    except FileNotFoundError:
        print(f"File not found: {urls_file}")
    except Exception as e:
        print(f"An error occurred: {e}")
if __name__ == "__main__":
    
    start_time = time.perf_counter()
    asyncio.run(main())
    elapsed_time = time.perf_counter() - start_time
    print(f"\nTemps d'exécution : {elapsed_time:.2f} secondes") # Temps d'exécution : 1.03 secondes