import asyncio
import time

async def compte_jusqu_a_10():
    for i in range(1,11):
        print(i)
        await asyncio.sleep(0.5)

if __name__ == "__main__":
    
    start_time = time.perf_counter()
    loop = asyncio.get_event_loop()

    tasks = [
        loop.create_task(compte_jusqu_a_10()),
        loop.create_task(compte_jusqu_a_10()),
        ]

    try:
        loop.run_until_complete(asyncio.wait(tasks))
        loop.close()

    except KeyboardInterrupt:
        print("\nC'est moins long pourtant !")
    
    elapsed_time = time.perf_counter() - start_time
    print(f"\nTemps d'exécution : {elapsed_time:.2f} secondes") # Temps d'exécution : 5.01 secondes
