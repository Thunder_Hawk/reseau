import asyncio
import random
from datetime import datetime
from colorama import Fore, Style
import logging

CLIENTS = {}

logging.basicConfig(filename='/var/log/chat_room/server.log', level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
console_handler.setFormatter(console_formatter)
logging.getLogger().addHandler(console_handler)

def generate_random_color():
    color_codes = [Fore.RED, Fore.GREEN, Fore.BLUE, Fore.YELLOW, Fore.MAGENTA, Fore.CYAN]
    return random.choice(color_codes)

async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    print(f"Client connected from {client_address}")

    addr = client_address
    if addr not in CLIENTS:
        data = await reader.read(1024)
        message = data.decode()
        if message.startswith("Hello|"):
            pseudo = message.split("|")[1]
            color = generate_random_color()
            print(f"{pseudo} has joined the chatroom!")
            logging.info(f'{pseudo} has joined the chatroom!')
            CLIENTS[addr] = {"r": reader, "w": writer, "pseudo": pseudo, "color": color}

            for client_addr, client_info in CLIENTS.items():
                if client_addr != addr:
                    client_writer = client_info["w"]
                    client_writer.write(f"{color}[Server]: {pseudo} a rejoint la chatroom{Style.RESET_ALL}\n".encode())
                    await client_writer.drain()

    try:
        while True:
            data = await reader.read(1024)
            if not data:
                break

            message = data.decode()

            if message == "":
                break

            timestamp = datetime.now().strftime("[%H:%M]")
            color = CLIENTS[addr]["color"]
            pseudo = CLIENTS[addr]["pseudo"]

            for client_addr, client_info in CLIENTS.items():
                if client_addr != addr:
                    client_writer = client_info["w"]
                    client_writer.write(f"{color}{timestamp} {pseudo} : {message}{Style.RESET_ALL}\n".encode())
                    await client_writer.drain()
                else :
                    client_writer = client_info["w"]
                    client_writer.write(f"Vous avez dit : {message}{Style.RESET_ALL}\n".encode())
                    await client_writer.drain()

            logging.info(f'{timestamp} {pseudo}: {message}')

    except asyncio.CancelledError:
        pass

    except Exception as e:
        print(f"Error handling client {addr}: {e}")

    finally:
        if addr in CLIENTS:
            pseudo = CLIENTS[addr]["pseudo"]
            color = CLIENTS[addr]["color"]
            timestamp = datetime.now().strftime("[%H:%M]")
            print(f"{pseudo} has left the chatroom!")
            logging.info(f'{timestamp} {pseudo} has left the chatroom!')

            for client_addr, client_info in CLIENTS.items():
                client_writer = client_info["w"]
                client_writer.write(f"{color}{timestamp} [Server]: {pseudo} a quitté la chatroom{Style.RESET_ALL}\n".encode())
                await client_writer.drain()

            del CLIENTS[addr]
            writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '127.0.0.1', 8888
    )

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        try:
            await server.serve_forever()
        except KeyboardInterrupt:
            print("\nServer interrupted. Exiting.")

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except:
        print("\nstopped")
