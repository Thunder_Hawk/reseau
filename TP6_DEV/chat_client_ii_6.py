import asyncio
import aioconsole
import re

async def user_input(writer):
    try:
        while True:
            #Your message (Ctrl+C to exit): 
            user_message = await aioconsole.ainput("")
            user_message = user_message if type(user_message) is str else await aioconsole.ainput("")
            writer.write(user_message.encode())
            await writer.drain()

    except asyncio.CancelledError:
        print("Server disconnected. Exiting.")
        exit()

async def receive_messages(reader):
    try:
        while True:
            data = await reader.read(1024)
            if not data:
                print("Server disconnected. Exiting.")
                exit()

            message = data.decode()
            print(message)

    except asyncio.CancelledError:
        pass

async def main():
    try:
        reader, writer = await asyncio.open_connection('127.0.0.1', 8888)

        pseudo = input("Choose your pseudo: ")
        pseudo = pseudo if type(pseudo) is str else input("Choose your pseudo: ")

        writer.write(f"Hello|{pseudo}".encode())
        await writer.drain()

        input_task = asyncio.create_task(user_input(writer))
        receive_task = asyncio.create_task(receive_messages(reader))

        await asyncio.gather(input_task, receive_task)

    except KeyboardInterrupt:
        print("\nExiting chat client.")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        if not writer.is_closing():
            writer.close()
            await writer.wait_closed()

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Connection stopped")