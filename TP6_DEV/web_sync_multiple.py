import sys
import time

from web_sync import get_content, write_content

def process_urls_from_file(file_path):
    with open(file_path, 'r') as file:
        urls = file.readlines()

    for url in urls:
        url = url.strip()
        if url:
            content = get_content(url)
            file_name = f"/tmp/web_{url.replace('https://', '').replace('/', '_')}"
            write_content(content, file_name)
            print(f"Content downloaded from {url} and saved to {file_name}")

if __name__ == "__main__":
    start_time = time.perf_counter()
    if len(sys.argv) != 2:
        print("Usage: python script.py <URLs_file>")
        sys.exit(1)

    urls_file = sys.argv[1]
    process_urls_from_file(urls_file)
    elapsed_time = time.perf_counter() - start_time
    print(f"\nTemps d'exécution : {elapsed_time:.2f} secondes") # Temps d'exécution : 4.57 secondes
