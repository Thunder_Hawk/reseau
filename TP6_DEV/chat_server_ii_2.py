import asyncio

async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    print(f"Client connected from {client_address}")

    data = await reader.read(1024)
    message = data.decode()

    print(f"Received message from {client_address}: {message}")

    response = f"Hello {client_address[0]}:{client_address[1]}"
    writer.write(response.encode())
    await writer.drain()

    print(f"Closing connection from {client_address}")
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '127.0.0.1', 8888
    )

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())