import asyncio
import aioconsole
from colorama import Fore, Style
import logging

logging.basicConfig(filename='/var/log/chat_room/client.log', level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

async def user_input(writer, pseudo):
    try:
        while True:
            user_message = await aioconsole.ainput("")
            formatted_message = f"{Fore.GREEN} {user_message}"
            writer.write(formatted_message.encode())
            await writer.drain()

    except asyncio.CancelledError:
        print("\nServer disconnected. Exiting.")
        exit()

async def receive_messages(reader):
    try:
        while True:
            data = await reader.read(1024)
            if not data:
                print("\nServer disconnected. Exiting.")
                exit()

            message = data.decode()
            print(format_message(message))

            logging.info(message)

    except asyncio.CancelledError:
        pass

def format_message(message):
    parts = message.split("]", 2)
    if len(parts) == 3:
        color, timestamp, content = parts
        return f"{color}{timestamp}]]{content}{Style.RESET_ALL}"
    else:
        return message

async def main():
    try:
        reader, writer = await asyncio.open_connection('127.0.0.1', 8888)

        pseudo = input("Choose your pseudo: ")
        pseudo = pseudo if type(pseudo) is str else input("Choose your pseudo: ")

        writer.write(f"Hello|{pseudo}".encode())
        await writer.drain()

        input_task = asyncio.create_task(user_input(writer, pseudo))
        receive_task = asyncio.create_task(receive_messages(reader))

        await asyncio.gather(input_task, receive_task)

    except :
        print("\nExiting chat client.")

    finally:
        if not writer.is_closing():
            writer.close()
            await writer.wait_closed()

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except:
        print("\nConnection stopped")
