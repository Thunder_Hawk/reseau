import time

def compte_jusqu_a_10():
    
    for i in range(1, 11):
        print(i)
        time.sleep(0.5)

def compte_2_fois():
    
    start_time = time.perf_counter()
    compte_jusqu_a_10()
    compte_jusqu_a_10()
    elapsed_time = time.perf_counter() - start_time
    print(f"\nTemps d'exécution : {elapsed_time:.2f} secondes") # Temps d'exécution : 10.02 secondes
    
if __name__ == "__main__" :
    try:
        compte_2_fois()
    except KeyboardInterrupt:
        print("\nJe sais, c'est trop long !")

