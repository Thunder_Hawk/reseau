import asyncio

CLIENTS = {}

async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    print(f"Client connected from {client_address}")

    addr = client_address
    if addr not in CLIENTS:
        CLIENTS[addr] = {}
        CLIENTS[addr]["r"] = reader
        CLIENTS[addr]["w"] = writer

    try:
        while True:
            data = await reader.read(1024)
            if not data:
                break

            message = data.decode()

            for client_addr, client_info in CLIENTS.items():
                if client_addr != addr:
                    client_writer = client_info["w"]
                    client_writer.write(f"{client_address[0]}:{client_address[1]} said: {message}".encode())
                    await client_writer.drain()

    except asyncio.CancelledError:
        pass

    finally:
        del CLIENTS[addr]
        print(f"Closing connection from {client_address}")
        writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '127.0.0.1', 8888
    )

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())