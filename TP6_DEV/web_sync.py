import requests
import sys

def get_content(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception(f"Failed to retrieve content from {url}. Status code: {response.status_code}")

def write_content(content, file):
    with open(file, 'w', encoding='utf-8') as f:
        f.write(content)

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: python script.py <URL>")
        sys.exit(1)

    url = sys.argv[1]
    content = get_content(url)

    file_path = "/tmp/web_page"
    write_content(content, file_path)

    print(f"Content downloaded from {url} and saved to {file_path}")
    

# (cours)  toto@fedora39  ~/Desktop/reseau/TP6_DEV   main ±  python web_sync.py https://www.ynov.com
# Content downloaded from https://www.ynov.com and saved to /tmp/web_page

# (base)  toto@fedora39  ~  cat /tmp/web_page | head -n 8 | tail -n 1
# <link href="https://static.vitrine.ynov.com" rel="preconnect" crossorigin>
