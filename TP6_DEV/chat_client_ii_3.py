import asyncio
import aioconsole

async def user_input(writer):
    while True:
        user_message = await aioconsole.ainput("Your message (Ctrl+C to exit): ")
        writer.write(user_message.encode())
        await writer.drain()

async def receive_messages(reader):
    while True:
        data = await reader.read(1024)
        if not data:
            break
        message = data.decode()
        print(f"Received message from server: {message}")

async def main():
    try:
        reader, writer = await asyncio.open_connection('127.0.0.1', 8888)

        input_task = asyncio.create_task(user_input(writer))
        receive_task = asyncio.create_task(receive_messages(reader))

        await asyncio.gather(input_task, receive_task)

    except KeyboardInterrupt:
        print("\nExiting chat client.")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        if not writer.is_closing():
            writer.close()
            await writer.wait_closed()

if __name__ == "__main__":
    asyncio.run(main())