import aiohttp
import aiofiles
import asyncio
import sys

async def get_content(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            if response.status == 200:
                return await response.text()
            else:
                raise Exception(f"Failed to retrieve content from {url}. Status code: {response.status}")

async def write_content(content, file):
    async with aiofiles.open(file, 'w', encoding='utf-8') as f:
        await f.write(content)
        await f.flush()

async def main():
    if len(sys.argv) != 2:
        print("Usage: python script.py <URL>")
        return

    url = sys.argv[1]
    content = await get_content(url)

    file_path = "/tmp/web_page"
    await write_content(content, file_path)

    print(f"Content downloaded from {url} and saved to {file_path}")

if __name__ == "__main__":
    asyncio.run(main())
    
# (cours)  toto@fedora39  ~/Desktop/reseau/TP6_DEV   main ±  python web_async.py https://www.google.com
# Content downloaded from https://www.google.com and saved to /tmp/web_page

# (base)  toto@fedora39  ~  cat /tmp/web_page | grep -o www.google.com | head -n 1
# www.google.com
