import socket

def main():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 8888))

    message = "Hello"
    client.sendall(message.encode())

    data = client.recv(1024)
    response = data.decode()

    print(f"Received response from server: {response}")

    client.close()

if __name__ == "__main__":
    main()