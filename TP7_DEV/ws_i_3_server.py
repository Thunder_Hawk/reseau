import asyncio
import websockets
import json
from pathlib import Path

HISTORY_FILE = "chat_history.json"

CLIENTS = {}

async def save_to_history(message):
    history = []
    
    if Path(HISTORY_FILE).exists():
        with open(HISTORY_FILE, "r") as file:
            history = json.load(file)

    history.append(message)

    with open(HISTORY_FILE, "w") as file:
        json.dump(history, file)

async def send_history(websocket):
    if Path(HISTORY_FILE).exists():
        with open(HISTORY_FILE, "r") as file:
            history = json.load(file)
            for message in history:
                await websocket.send(message.encode())

async def handle_client(websocket, path):
    client_address = websocket.remote_address
    print(f"Client connected from {client_address}")

    await send_history(websocket)

    if websocket not in CLIENTS:
        data = await websocket.recv()
        message = data.decode()
        if message.startswith("Hello|"):
            pseudo = message.split("|")[1]
            print(f"{pseudo} has joined the chatroom!")
            CLIENTS[websocket] = {"pseudo": pseudo}

            for ws, info in CLIENTS.items():
                if ws != websocket:
                    await ws.send(f"Annonce: {pseudo} a rejoint la chatroom".encode())

    try:
        while True:
            data = await websocket.recv()
            if not data:
                break

            message = data.decode()

            if message == "":
                break

            for ws, info in CLIENTS.items():
                if ws != websocket:
                    client_pseudo = info["pseudo"]
                    await ws.send(f"{client_pseudo} a dit : {message}".encode())

            await save_to_history(f"{pseudo} a dit : {message}".encode())

    except websockets.exceptions.ConnectionClosedError:
        pass

    finally:
        if websocket in CLIENTS:
            pseudo = CLIENTS[websocket]["pseudo"]
            print(f"{pseudo} has left the chatroom!")

            for ws, info in CLIENTS.items():
                if ws != websocket:
                    await ws.send(f"Annonce: {pseudo} a quitté la chatroom".encode())

            del CLIENTS[websocket]
            await websocket.close()

async def main():
    server = await websockets.serve(
        handle_client, '127.0.0.1', 8888
    )

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.wait_closed()

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("\nServer closed!")