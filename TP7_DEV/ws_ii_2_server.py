import asyncio
import websockets
import redis.asyncio as redis
import json

REDIS_HOST = "10.1.1.12"
REDIS_PORT = 6379
REDIS_DB = 0
CLIENT_PSEUDO = None

class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return obj.decode('utf-8')
        return super().default(obj)

async def save_to_redis(client_id, pseudo, client):
    try:
        
        infos = {'pseudo': pseudo, 'connected': True}
        print("----")
        print(client_id)
        print(infos)
        print("----")

        await client.hset('clients', client_id, json.dumps(infos))
        
        c = await client.hgetall("clients")
        print(c)
        print(type(c))
        
        #ajouter au dico au lieu de set
        # await client.sadd('clients', client_id, json.dumps({'pseudo': pseudo, 'connected': True}))
        CLIENT_PSEUDO  = pseudo

        
    except Exception as e:
        print(f"Error saving to Redis: {e}")

async def get_clients_from_redis(client):
    try:
        clients = await client.hgetall('clients')
    except Exception as e:
        print(f"Error getting clients from Redis: {e}")

    result = {}
    for key, value in clients.items():
        try:
            # demain !
            decoded_key = key.decode('utf-8')
            decoded_value = json.loads(value.decode('utf-8')) if value else None
            result[decoded_key] = decoded_value
        except json.decoder.JSONDecodeError as e:
            print(f"Error decoding JSON for key {decoded_key}: {e}")

    return result

async def handle_client(websocket, path):
    client_address = websocket.remote_address
    print(f"Client connected from {client_address}")

    redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, encoding='utf-8')
    clients = await get_clients_from_redis(redis_client)

    await websocket.send(json.dumps(clients, cls=CustomEncoder).encode())

    data = await websocket.recv()
    message = data.decode()
    if message.startswith("Hello|"):
        pseudo = message.split("|")[1]
        await save_to_redis(str(client_address), pseudo, redis_client)
        print(f"{pseudo} has joined the chatroom!")

        for client_id, client_info in clients.items():
            if client_id != CLIENT_PSEUDO and client_info['connected']:
                await websocket.send(f"Annonce: {pseudo} a rejoint la chatroom".encode())
                
                # ecrase au lieu de ajouter

    try:
        while True:
            data = await websocket.recv()
            if not data:
                break

            message = data.decode()

            if message == "":
                break

            clients = await get_clients_from_redis(redis_client)
            for client_id, client_info in clients.items():
                if client_id != CLIENT_PSEUDO and client_info['connected']:
                    await websocket.send(f"{client_info['pseudo']} a dit : {message}")

    except websockets.exceptions.ConnectionClosedError:
        pass
    
    except websockets.exceptions.ConnectionClosedOK:
        
        pseudo = (await get_clients_from_redis(redis_client)).get(str(client_address), {}).get('pseudo', "")
        print(f"{pseudo} has left the chatroom!")

        clients = await get_clients_from_redis(redis_client)
        for client_id, client_info in clients.items():
            if client_id != CLIENT_PSEUDO and client_info['connected']:
                await websocket.send(f"Annonce: {pseudo} a quitté la chatroom".encode())
                        # get the client > set client_info to disconneted

        await redis_client.hdel('clients', str(client_address))
        await redis_client.aclose()

        await websocket.close()

async def main():
    server = await websockets.serve(handle_client, '10.1.1.12', 13337)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await asyncio.Future()  # run forever

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("\nServer closed!")
        
        # websockets.exceptions.ConnectionClosedOK > erreur de deco