import asyncio
import websockets


async def server_handler(websocket, path):
    
    while True:
        
        try:
            
            message = await websocket.recv()
            print(f"Received message from client: {message}")
            response = "Ack"
            await websocket.send(response)
            
        except websockets.exceptions.ConnectionClosed:
            break


if __name__ == "__main__" :
    
    start_server = websockets.serve(server_handler, "localhost", 8765)
    
    try:
        
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    except KeyboardInterrupt:
        print("\n Exiting")
