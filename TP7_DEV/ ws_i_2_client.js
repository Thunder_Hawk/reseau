const WebSocket = require('ws');

const uri = "ws://localhost:8765";
const socket = new WebSocket(uri);

socket.on('open', (event) => {
    console.log('WebSocket connection opened, write your message :', event);
});

socket.on('message', (data) => {
    const message = data.toString();
    console.log('Received message from server:', message);
});

const sendMessage = (message) => {
    socket.send(message);
    console.log('Sent message to server:', message);
};


const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on('line', (input) => {
    if (input.toLowerCase() === 'exit') {
        socket.close();
        console.log('WebSocket connection closed.');
        rl.close();
    } else {
        sendMessage(input);
    }
});

process.on('SIGINT', () => {
    socket.close();
    console.log('\nWebSocket connection closed.');
    process.exit();
});