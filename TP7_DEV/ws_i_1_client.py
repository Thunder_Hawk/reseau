import asyncio
import websockets


async def client_handler():
    
    uri = "ws://localhost:8765"
    
    try:
        
        async with websockets.connect(uri) as websocket:
            
            while True:
                
                user_input = input("Enter a message to send to the server (or 'exit' to quit): ")
                
                if user_input.lower() == 'exit':
                    break

                await websocket.send(user_input)
                print(f"Sent message to server: {user_input}")

                response = await websocket.recv()
                print(f"{response}")

    except websockets.exceptions.ConnectionClosedError:
        print("Connection closed by the server.")


if __name__ == "__main__" :
    
    try:
        
        asyncio.run(client_handler())

    except KeyboardInterrupt:
        print("\n Exiting")

