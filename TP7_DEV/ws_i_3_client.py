import asyncio
import aioconsole
import websockets

async def user_input(websocket):
    while True:
        user_message = await aioconsole.ainput("")
        await websocket.send(user_message.encode())

async def receive_messages(websocket):
    try:
        while True:
            data = await websocket.recv()
            if not data:
                print("\n   Server disconnected. Exiting.")
                return
            print(data.decode())
    except asyncio.CancelledError:
        pass

async def main():
    uri = "ws://10.1.1.12:13337"

    async with websockets.connect(uri) as websocket:
        pseudo = input("Choose your pseudo: ")
        # Send a properly formatted "Hello" message to the server
        await websocket.send(f"Hello|{pseudo}".encode())

        input_task = asyncio.create_task(user_input(websocket))
        receive_task = asyncio.create_task(receive_messages(websocket))

        await asyncio.gather(input_task, receive_task)

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("\nClient closed!")