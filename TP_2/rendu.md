# TP2 : Environnement virtuel

## Compte-rendu


☀️ **Sur node1.lan1.tp2**

```bash
[user@node1Lan1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:40:6e:46 brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe40:6e46/64 scope link 
       valid_lft forever preferred_lft forever

```

```bash
[user@node1Lan1 ~]$ ip r s
default via 10.1.1.254 dev enp0s3 
10.1.1.0/24 dev enp0s3 proto kernel scope link src 10.1.1.11 metric 100 

```

```bash
[user@node1Lan1 ~]$ ping 10.1.2.12
PING 10.1.2.12 (10.1.2.12) 56(84) bytes of data.
64 bytes from 10.1.2.12: icmp_seq=1 ttl=63 time=1.12 ms
64 bytes from 10.1.2.12: icmp_seq=2 ttl=63 time=1.11 ms
^C
--- 10.1.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.114/1.117/1.121/0.003 ms

```

```bash
[user@node1Lan1 ~]$ traceroute 10.1.2.12
traceroute to 10.1.2.12 (10.1.2.12), 30 hops max, 60 byte packets
 1  _gateway (10.1.1.254)  1.610 ms  1.535 ms  1.468 ms
 2  10.1.2.12 (10.1.2.12)  1.358 ms !X  1.284 ms !X  1.158 ms !X

```

☀️ **Sur router.tp2**

```bash
[user@routeur ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=16.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=16.6 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 16.552/16.685/16.819/0.133 ms

```

```bash
[user@routeur ~]$ ping ynov.com
PING ynov.com (104.26.10.233) 56(84) bytes of data.
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=1 ttl=63 time=17.3 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=2 ttl=63 time=20.2 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 17.317/18.742/20.167/1.425 ms

```

☀️ **Accès internet LAN1 et LAN2**

```bash
[user@node2Lan1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=61 time=42.4 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=61 time=9.88 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 9.884/26.136/42.388/16.252 ms

```

```bash
[user@node2Lan1 ~]$ ping example.com
PING example.com (93.184.216.34) 56(84) bytes of data.
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=1 ttl=61 time=96.6 ms
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=2 ttl=61 time=94.0 ms
^C
--- example.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 93.985/95.286/96.588/1.301 ms

```
☀️ **Sur dhcp.lan1.tp2**

```bash
[user@dhcpLan1 ~]$ sudo dnf install dhcp-server -y

```

```bash
[user@dhcpLan1 ~]$ sudo cat /etc/dhcp/dhcpd.conf

subnet 10.1.1.0 netmask 255.255.255.0 {
    range 10.1.1.100 10.1.1.200;
    option routers 10.1.1.254;
    option domain-name-servers 1.1.1.1;
}

```

```bash
[user@dhcpLan1 ~]$ sudo systemctl status dhcpd | grep active

     Active: active (running) since Thu 2023-10-19 18:33:26 CEST; 39s ago

```

☀️ **Sur node1.lan1.tp2**

```bash
[user@node1Lan1 ~]$ ip a | grep dynamic
    inet 10.1.1.100/24 brd 10.1.1.255 scope global secondary dynamic noprefixroute enp0s3


```

```bash
[user@node1Lan1 ~]$ ip r s
default via 10.1.1.254 dev enp0s3 proto dhcp src 10.1.1.100 metric 100 

```

```bash
[user@node1Lan1 ~]$ ping 10.1.2.11
PING 10.1.2.11 (10.1.2.11) 56(84) bytes of data.
64 bytes from 10.1.2.11: icmp_seq=1 ttl=63 time=1.04 ms
64 bytes from 10.1.2.11: icmp_seq=2 ttl=63 time=1.98 ms
^C
--- 10.1.2.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.036/1.509/1.982/0.473 ms

```

## 2. Web web web

☀️ **Sur web.lan2.tp2**

```bash
[user@webLan2 ~]$ sudo dnf install nginx -y

```

```bash
[user@webLan2 ~]$ sudo mkdir /var/www
[user@webLan2 ~]$ sudo mkdir /var/www/site_nul
[user@webLan2 ~]$ sudo vim /var/www/site_nul/index.html

[user@webLan2 ~]$ sudo chown -R nginx:nginx /var/www

```

```bash
[user@webLan2 ~]$ sudo cat /etc/nginx/conf.d/site_nul.conf
server {
    listen 80;
    server_name site_nul.tp2;

    location / {
        root /var/www/site_nul;
        index index.html;
    }
}

```

```bash
[user@webLan2 ~]$ sudo systemctl status nginx | grep active
     Active: active (running) since Thu 2023-10-19 19:18:09 CEST; 4min 18s ago

```

```bash
[user@webLan2 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[user@webLan2 ~]$ sudo firewall-cmd --reload
success

```

```bash
[user@webLan2 ~]$ ss -altnp4 | grep 80
LISTEN 0      511          0.0.0.0:80        0.0.0.0:* 

```

```bash
[user@webLan2 ~]$ sudo firewall-cmd --list-all | grep 80
  ports: 80/tcp

```

☀️ **Sur node1.lan1.tp2**

```bash
[user@node1Lan1 ~]$ curl site_nul.tp2 | grep 2050
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3777  100  3777    0     0   263k      0 --:--:-- --:--:-- --:--:--  263k
      <p>©Copyright 2050 par personne. Tous droits reversés.</p>

```

![That's all](/pics/folks.jpg)